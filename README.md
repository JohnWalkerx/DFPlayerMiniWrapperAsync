# DFPlayerMiniWrapperAsync

Wrapper to control DFPlayer Mini from DFRobot asynchronous with ESP8266.
It uses the original [DFRobotDFPlayerMini library](https://github.com/DFRobot/DFRobotDFPlayerMini).

## Why this library?

The reason I wrote this wrapper is that the original library blocks while it waits for the answer of the DFPlayer.
For the ESP8266 this will not work properly.

So i tried to make it non-blocking.

There are still bugs and missing functionality. Also it's a little bit dirty.
But I doesn't have the time to do it correctly. For my purpose it is enough.

## License

The source code is licensed under the [MIT Licence](LICENCE).

## Contribution

Feel free to contribute. :-)
