#ifndef DFPLAYERMINIWRAPPER_H
#define DFPLAYERMINIWRAPPER_H

#include <DFRobotDFPlayerMini.h>
#include <queue>
#include <string>

enum E_Command
{
    EvCommandUndefined = 0,
    EvCommandPlay,
    EvCommandStop,
    EvCommandNext,
    EvCommandPrevious,
    EvCommandSetVolume,
    EvCommandVolumeUp,
    EvCommandVolumeDown,
    EvCommandLoop,
    EvCommandSleep,
    EvCommandReset,
    EvCommandStart,
    EvCommandPause,
    EvCommandSetEq,
    EvCommandEnableLoop,
    EvCommandDisableLoop
};

typedef std::pair<E_Command, int> Command;

class DFPlayerMiniWrapper
{
public:
    DFPlayerMiniWrapper();
    ~DFPlayerMiniWrapper();

    bool begin(Stream &stream, bool reset = true);
    void setDebugSerial(Stream &stream);

    void next();
    void previous();
    void play(int fileNumber = 1);
    void loop(int fileNumber);

    void sleep();
    void reset();
    void start();
    void pause();
    void stop();

    void volumeUp();
    void volumeDown();
    void setVolume(uint8_t volume);
    void setEq(uint8_t eq);

    void enableLoop();
    void disableLoop();

    void processCommunication();

private:
    void addCommand(E_Command command, int parameter);
    Command nextCommand();
    void sendCommand(E_Command command, int parameter);

    String commandAsString(E_Command command);
    void debugLog(const String &logMsg);

    DFRobotDFPlayerMini m_dfPlayer;
    std::queue<Command> m_pendingCommands;
    bool m_isSending {false};
    int m_lastSending;
    int m_sendingWaitIntervall {300};
    Stream *m_debugStream {nullptr};
};

#endif // DFPLAYERMINIWRAPPER_H