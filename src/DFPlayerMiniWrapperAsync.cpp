#include "DFPlayerMiniWrapperAsync.h"

DFPlayerMiniWrapper::DFPlayerMiniWrapper(/* args */) : m_lastSending(millis())
{
}

DFPlayerMiniWrapper::~DFPlayerMiniWrapper()
{
}

bool DFPlayerMiniWrapper::begin(Stream &stream, bool reset)
{
    debugLog("Set serial to DFPlayer");
    return m_dfPlayer.begin(stream, false, reset);
}

void DFPlayerMiniWrapper::setDebugSerial(Stream &stream)
{
    m_debugStream = &stream;
}

void DFPlayerMiniWrapper::next()
{
    addCommand(EvCommandNext, {});
}

void DFPlayerMiniWrapper::previous()
{
    addCommand(EvCommandPrevious, {});
}

void DFPlayerMiniWrapper::play(int fileNumber)
{
    addCommand(EvCommandPlay, fileNumber);
}

void DFPlayerMiniWrapper::loop(int fileNumber)
{
    addCommand(EvCommandLoop, fileNumber);
}

void DFPlayerMiniWrapper::sleep()
{
    addCommand(EvCommandSleep, {});
}

void DFPlayerMiniWrapper::reset()
{
    addCommand(EvCommandReset, {});
}

void DFPlayerMiniWrapper::start()
{
    addCommand(EvCommandStart, {});
}

void DFPlayerMiniWrapper::pause()
{
    addCommand(EvCommandPause, {});
}

void DFPlayerMiniWrapper::stop()
{
    addCommand(EvCommandStop, 0);
}

void DFPlayerMiniWrapper::volumeUp()
{
    addCommand(EvCommandVolumeUp, {});
}

void DFPlayerMiniWrapper::volumeDown()
{
    addCommand(EvCommandVolumeDown, {});
}

void DFPlayerMiniWrapper::setVolume(uint8_t volume)
{
    addCommand(EvCommandSetVolume, static_cast<int>(volume));
}

void DFPlayerMiniWrapper::setEq(uint8_t eq)
{
    addCommand(EvCommandSetEq, static_cast<int>(eq));
}

void DFPlayerMiniWrapper::enableLoop()
{
    addCommand(EvCommandEnableLoop, {});
}

void DFPlayerMiniWrapper::disableLoop()
{
    addCommand(EvCommandDisableLoop, {});
}

void DFPlayerMiniWrapper::processCommunication()
{
    m_dfPlayer.available();

    if (millis() - m_lastSending > m_sendingWaitIntervall &&
        !m_dfPlayer._isSending)
    {
        m_lastSending = millis();
        m_isSending = false;
    }

    if (!m_isSending)
    {
        const auto next = nextCommand();
        sendCommand(next.first, next.second);
    }
}

void DFPlayerMiniWrapper::addCommand(E_Command command, int parameter)
{
    m_pendingCommands.push({command, parameter});
    debugLog("Add command '" + commandAsString(command) + "' to queue.");
}

void DFPlayerMiniWrapper::sendCommand(E_Command command, int parameter)
{
    m_isSending = true;

    if (command == EvCommandUndefined)
    {
        return;
    }

    debugLog("Send Command: '" + commandAsString(command) + "' with parameter: " + String(parameter));

    switch (command)
    {
    case EvCommandPlay:
        m_dfPlayer.play(parameter);
        break;

    case EvCommandStop:
        m_dfPlayer.stop();
        break;

    case EvCommandNext:
        m_dfPlayer.next();
        break;

    case EvCommandPrevious:
        m_dfPlayer.previous();
        break;

    case EvCommandSetVolume:
        m_dfPlayer.volume(static_cast<uint8_t>(parameter));
        break;

    case EvCommandVolumeUp:
        m_dfPlayer.volumeUp();
        break;

    case EvCommandLoop:
        m_dfPlayer.loop(parameter);
        break;

    case EvCommandSleep:
        m_dfPlayer.sleep();
        break;

    case EvCommandReset:
        m_dfPlayer.reset();
        break;

    case EvCommandStart:
        m_dfPlayer.start();
        break;

    case EvCommandPause:
        m_dfPlayer.pause();
        break;

    case EvCommandSetEq:
        m_dfPlayer.EQ(static_cast<uint8_t>(parameter));
        break;

    case EvCommandEnableLoop:
        m_dfPlayer.enableLoop();
        break;

    case EvCommandDisableLoop:
        m_dfPlayer.disableLoop();
        break;

    default:
        break;
    }
}

String DFPlayerMiniWrapper::commandAsString(E_Command command)
{
    switch (command)
    {
    case EvCommandUndefined:
        return "EvCommandUndefined";
    case EvCommandPlay:
        return "EvCommandPlay";
    case EvCommandStop:
        return "EvCommandStop";
    case EvCommandNext:
        return "EvCommandNext";
    case EvCommandPrevious:
        return "EvCommandPrevious";
    case EvCommandSetVolume:
        return "EvCommandSetVolume";
    case EvCommandVolumeUp:
        return "EvCommandVolumeUp";
    case EvCommandVolumeDown:
        return "EvCommandVolumeDown";
    case EvCommandLoop:
        return "EvCommandLoop";
    case EvCommandSleep:
        return "EvCommandSleep";
    case EvCommandReset:
        return "EvCommandReset";
    case EvCommandStart:
        return "EvCommandStart";
    case EvCommandPause:
        return "EvCommandPause";
    case EvCommandSetEq:
        return "EvCommandSetEq";
    case EvCommandEnableLoop:
        return "EvCommandEnableLoop";
    case EvCommandDisableLoop:
        return "EvCommandDisableLoop";

    default:
        return "";
        break;
    }
}

void DFPlayerMiniWrapper::debugLog(const String &logMsg)
{
    if (!m_debugStream)
    {
        return;
    }

    String msg = String(millis()) + ": DFMiniWrapper: ";
    msg.concat(logMsg);
    (*m_debugStream).println(msg);
}

Command DFPlayerMiniWrapper::nextCommand()
{
    if (m_pendingCommands.empty())
    {
        return {EvCommandUndefined, 0};
    }

    const auto next = m_pendingCommands.front();
    m_pendingCommands.pop();
    return next;
}
